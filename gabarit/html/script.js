// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);
// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

//
// smoothscroll between anchors
//
const anchors = document.querySelectorAll('a[href^="#"]');
for (const anchor of anchors) {
  anchor.addEventListener('click', (e) => {
    const hash = anchor.getAttribute('href');
    const destination = document.getElementById(hash.substring(1));
    destination.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
    window.history.pushState(null, null, hash);
    e.preventDefault();
  });
}

//
// Table button
//
const btnTable = document.querySelector('.button--table');
const table = document.querySelector('.table');
const btnTablePlus = document.querySelector('.button--table .plus');
const btnTableMinus = document.querySelector('.button--table .minus');
btnTable.addEventListener('click', (e) => {
  e.preventDefault();
  btnTable.blur();
  btnTablePlus.classList.toggle('hide');
  btnTableMinus.classList.toggle('hide');
  table.classList.toggle('show');
});

//
// Dark mode
//
const btnDark = document.querySelector('.button--dark');
let darkModeToggle = false;
const darkModeTheme = localStorage.getItem('theme');

if (darkModeTheme) {
  document.documentElement.setAttribute('data-theme', darkModeTheme);

  if (darkModeTheme === 'dark') {
    darkModeToggle = true;
  }
}

function darkModeSwitch() {
  if (!darkModeToggle) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
    darkModeToggle = true;
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
    darkModeToggle = false;
  }
}

btnDark.addEventListener('click', darkModeSwitch);

//
// Return to top
//
const scrollBtn = document.querySelector('.button--top');
const targetHeader = document.querySelector('header');

function callback(entries) {
  entries.forEach((entry) => {
    // only show the scroll to top button when the heading is out of view
    if (!entry.isIntersecting) {
      scrollBtn.classList.add('show');
      btnDark.classList.add('show');
    } else {
      scrollBtn.classList.remove('show');
      btnDark.classList.remove('show');
    }
    scrollBtn.dataset.show = (!entry.isIntersecting).toString();
    btnDark.dataset.show = (!entry.isIntersecting).toString();
  });
}

scrollBtn.addEventListener('click', () => {
  document.documentElement.scrollTo({ top: 0, behavior: 'smooth' });
});

const observer = new IntersectionObserver(callback);
observer.observe(targetHeader);
