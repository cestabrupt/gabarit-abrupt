--- Script lua pour pallier une erreur de citeproc avec pandoc qui place
--- deux fois un point final après "Ibid." ou "op. cit." en fin de ligne.
--- L'erreur ne semple plus exister à partir de la version 2.14.2 de pandoc.
function Inlines (inlines)
  for i = 1, #inlines-1 do
    if inlines[i].t == 'Cite'
    and inlines[i].content[1].t == 'Str' and inlines[i].content[1].text == ''
    and inlines[i].content[2].t == 'Emph'
    and inlines[i].content[3].t == 'Str' and inlines[i].content[3].text == ''
    then
       inlines[i+1] = pandoc.Emph {pandoc.Str('')}
    end
  end
return inlines
end

-- function Emph(elem)
--     return pandoc.walk_inline(elem, {
--         Inlines = function(elem)
--           -- if el.t == 'Str' and el.text == 'Ibid.'
--           -- then
--           for i = 1, #elem do
--             if elem[i].t == 'Str' and elem[i].text == 'Ibid.'
--             -- and elem[i+1].t == 'Str' and inlines[i+1].text == ''
--             then
--               elem[i] = pandoc.Str 'Ibid'
--             end
--           end
--           return elem
--         end })
-- end
