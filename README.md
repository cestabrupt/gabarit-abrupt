LE PROJET CONTINUE [SUR UNE AUTRE FORGE](https://codeberg.org/abrupt/gabarit-abrupt).

---
---
---

# Gabarit Abrüpt

Le Gabarit Abrüpt est un outil qui vous offre la possibilité de créer facilement un livre numérique ou papier.

<p align="center">
  <img src="./gabarit/gabarit-abrupt.jpg" alt="Gabarit Abrüpt">
</p>

[[_TOC_]]

## Introduction

Le Gabarit Abrüpt utilise la puissance de l’outil Pandoc afin de convertir un texte source brut en de nombreux formats. Son but est la production de livres (papier et numérique) avec une attention portée sur la facilité d’utilisation et sur la qualité typographique des ouvrages.

Il utilise notamment LuaTeX pour produire des fichiers PDF prêts à être imprimés (par ex. auprès d’un service d’impression à la demande).

Les différents gabarits sont adaptables et ils peuvent aisément devenir, grâce à Pandoc et TeX, un modèle pour une thèse scientifique, une affiche, un roman graphique, une présentation HTML dynamique, etc.

### Dépendances

* [Pandoc](http://pandoc.org/)
* [Citeproc](https://github.com/jgm/citeproc/)
* [LuaTeX](http://www.luatex.org/)

Il est possible de faire fonctionner Pandoc et LuaTeX sur Linux, Mac OS X et Windows.

### Installation

Pour installer Pandoc, voir la [documentation officielle](http://pandoc.org/installing.html).

Pour installer LuaTeX, il faut installer une distribution LaTeX afin de disposer de Lua et des différents paquets nécessaires (il s’agit néanmoins d’une installation conséquente allant de 1 à 4 GB). Nous vous recommandons [TeX Live](https://tug.org/texlive/), mais il est également possible d’utiliser [MacTeX](https://tug.org/mactex/) pour les utilisateurs de Mac OS X ou [MikTeX](https://miktex.org/) pour les utilisateurs de Windows.

Les différents gabarits fonctionnent avec des versions à jour des différents programmes, notamment de la distribution LaTeX.

Vous pouvez ensuite télécharger ce répertoire ou simplement le cloner avec git. Il est peut-être nécessaire de vérifier que le script `livre.sh` (et `gabarit/ebook/modification.sh`) soit exécutable.

## Utilisation du Gabarit Abrüpt

L’utilisation de ces gabarits est automatisée grâce à un *Makefile* et à différentes variables.

Différents exemples de livres se trouvent dans le dossier `exemples` (ils ont été produits pour illustrer l'utilisation des différentes commandes disponibles) ; s’y trouvent également des sources possibles afin d’effectuer d’autres tests.

### Utilisation générale des gabarits

Des détails sur les différentes options se trouvent également dans les commentaires des différents gabarits.

Des filtres Lua seront également utilisés pour améliorer le rendu final de certains formats, comme avec le script `gabarit/typographie.lua` qui opère certaines transformations des caractères utilisés afin d'obtenir une plus grande justesse typographique.

#### Sources

Le texte source (dans le dossier `texte`) utilise le langage de balisage léger Markdown (le plus souvent utilisant l'extension `.md`), avec [les quelques augmentations de Pandoc](https://pandoc.org/MANUAL.html#pandocs-markdown). Il est néanmoins possible d’utiliser un autre langage de balisage léger en modifiant les commandes dans le fichier `Makefile`.

#### Titres

Les différents titres dans la syntaxe Markdown (*ATX-style header* utilisant les `#`) seront numérotés ; ils peuvent néanmoins devenir non numérotés lors de l’ajout en fin de ligne du titre de `{-}`. Dans le cas d’une utilisation du script `livre.sh`, Pandoc convertira les fichiers bruts en transformant automatiquement les `{-}` par `{#identifier .unnumbered}`.

Il est également possible d’ajouter au sein de la syntaxe Markdown du code HTML ou LaTeX ; Pandoc les interprétera en fonction des conversions nécessaires.

#### Bibliographie

Pandoc permet d’ajouter [des citations](https://pandoc.org/MANUAL.html#citations) et de faire référence à une bibliographie grâce à [Citeproc](https://github.com/jgm/citeproc/). Dans les différents gabarits, des notes de bas de page seront ajoutées ainsi qu'une bibliographie à la fin du fichier.

La bibliographie est gérée par Citeproc, ce qui permet une certaine facilité d’utilisation et une adaptation rapide aux différents formats envisagés. Si vous souhaitez une alternative pour LaTeX, nous vous conseillons l’utilisation de Biblatex (avec Biber).

La bibliographie est composée non de toutes les entrées du [fichier BIB](bibliographie/sources/bibliographie.bib) (fichier source contenant la bibliographie), mais de tous les documents cités dans le texte ; il est possible d’ajouter à la bibliographie des documents non cités en les ajoutant dans les métadonnées du document avec le champ `nocite` comme le précise la documentation Pandoc.

Le [style CSL utilisé](bibliographie/styles/abrupt.csl) gère les "ibid.", "op. cit.", etc. ; il a été composé par nos soins selon nos préférences éditoriales. D'autres styles libres peuvent être découverts et édités grâce à [l'outil](http://editor.citationstyles.org/) fourni par Citation Style Language. Pour utiliser un autre style, il faut placer le fichier CSL dans le dossier `bibliographie/styles/` et indiquer son nom dans le fichier `Makefile`.

Notre *style Abrüpt* utilise des entrées et des champs Biblatex particuliers pour gérer une grande diversité de sources bibliographiques. Vous retrouverez les résultats de l'application de ce style dans les [exemples produits](exemples/). Il est possible de tester d'autres [entrées et champs Biblatex](http://tug.ctan.org/info/biblatex-cheatsheet/biblatex-cheatsheet.pdf), mais il est nécessaire de tenir compte de la conversion opérée selon les [spécifications CSL](https://docs.citationstyles.org/en/stable/specification.html) ; pour les découvrir plus aisément, il est possible d'utiliser la commande suivante : `pandoc bibliographie.bib -t csljson -o bibliographie.json`.

Voici les principales possibilités d'entrées et de champs Biblatex avec notre *style Abrüpt* (certains champs sont optionnels, notamment `translator`, `edition`, `series`, les différents `subtitle`, `note`, `pubstate`, `keywords`, `volume` ou encore `origdate`) :

```tex
% Un livre
@Book{test1,
  author = {Prénom Nom},
  title = {Un titre de livre},
  subtitle = {sous-titre du livre},
  edition = {2},
  translator = {Prénom Nom du Traducteur},
  location = {Le Lieu},
  publisher = {Nom de l'Éditeur},
  series = {Le nom d'une collection},
  date = {2001},
  note = {Une note, par exemple le Prénom Nom du commentateur du livre},
  pubstate = {livre numérique},
  keywords = {clef, autre},
}

% Un article dans une revue
@Article{test2,
  author={Prénom Nom},
  title={Titre de l'article},
  subtitle = {sous-titre de l'article},
  journaltitle={revue de métaphysique politique},
  journalsubtitle={sous-titre de la revue},
  translator = {Prénom {Nom du Traducteur}},
  volume={4},
  number={2},
  date={2021},
  pages={1-100},
  note = {commentaire en plus sur l'article},
  pubstate = {type de publication},
  keywords = {clef, autre},
}

% Une partie indépendante d'un livre telle une préface
% InBook est rarement utilisé (usage plus fréquent d'InCollection)
@InBook{test3,
  author = {Prénom Nom},
  title = {Introduction au livre},
  subtitle = {sous-titre à l'introduction},
  bookauthor = {Prénom {Nom de l'Auteur du livre}},
  booktitle = {Le titre du livre},
  booksubtitle = {sous-titre du livre},
  translator = {Prénom {Nom du Traducteur}},
  pages = {1-42},
  location = {Le Lieu},
  publisher = {Maison d'édition},
  series = {le nom d'une collection},
  date = {2008},
  note = {commentaire en plus sur l'article},
  pubstate = {livre numérique},
  keywords = {clef, autre},
}

% Un texte indépendant dans un ouvrage contenant plusieurs textes
@InCollection{test4,
  author = {Prénom Nom},
  title = {Titre de la participation},
  subtitle = {sous-titre de la participation},
  editor = {Prénom Nom},
  booktitle = {Titre de l'ouvrage collectif},
  booksubtitle = {sous-titre de l'ouvrage collectif},
  translator = {Prénom {Nom du Traducteur}},
  location = {Le Lieu},
  publisher = {Flammarion},
  series = {le nom d'une collection},
  pages = {1-42},
  date = {2008},
  note = {commentaire en plus sur l'article},
  pubstate = {livre numérique},
  keywords = {clef, autre},
}

% Un livre indépendant dans un livre
% par exemple un titre dans des œuvres complètes
@BookInBook{test5,
  type = {recueil},
  author = {Prénom Nom},
  bookauthor = {Prénom Nom},
  title = {Livre dans un recueil d'un seul auteur},
  subtitle = {sous-titre du livre dans le recueil},
  editor = {Prénom {Nom de l'Éditeur}},
  translator = {Prénom {Nom du Traducteur}},
  booktitle = {Œuvres complètes par exemple},
  booksubtitle = {sous-titre aux œuvres complètes},
  location = {Le Lieu},
  pages = {1-42},
  publisher = {Maison d'édition},
  series = {le nom d'une collection},
  volume = {42},
  origdate = {1848},
  date = {2008},
  note = {commentaire de l'ouvrage par Madame Anonyme},
  pubstate = {livre numérique},
  keywords = {clef, autre},
}

% Un site Internet
@Online{test6,
  author = {Prénom Nom},
  title = {Titre de l'article sur un site},
  subtitle = {sous-titre},
  url = {https://lenomdusite.cc/titredelarticlesurunsite},
  date = {2020-04-01},
  urldate = {2021-05-01},
  publisher = {Le Nom Du Site},
  translator = {Prénom {Nom du Traducteur}},
  note = {une note sur l'article},
  keywords = {clef, autre},
}
```

### Structure

```
├── bibliographie
│   ├── sources
│   │   └── bibliographie.bib
│   └── styles
│       └── abrupt.csl
├── dist
│   └── gabarit-abrupt.html
├── exemples
│   ├── couverture.pdf
│   ├── gabarit-abrupt.epub
│   ├── gabarit-abrupt.html
│   ├── gabarit-abrupt.md
│   ├── gabarit-abrupt.odt
│   ├── gabarit-abrupt.pdf
│   ├── gabarit-ancienne-version-avec-notes-de-fin.pdf
│   ├── gabarit-ancienne-version-essai.pdf
│   ├── gabarit-ancienne-version-recueil-de-poesie.pdf
│   ├── gabarit-ancienne-version-roman.pdf
│   └── textes_aternatifs
│       ├── essai
│       ├── poesie
│       └── roman
├── gabarit
│   ├── code-barres.png
│   ├── couverture.jpg
│   ├── couverture.tex
│   ├── ebook
│   │   ├── couverture_ebook.jpg
│   │   ├── ebook.css
│   │   ├── fonts
│   │   ├── gabarit.epub2
│   │   └── modification.sh
│   ├── html
│   │   ├── fonts
│   │   ├── marquetypographique.jpg
│   │   ├── normalize.css
│   │   ├── script.js
│   │   ├── web.css
│   │   └── web.html
│   ├── letterspacing.lua
│   ├── livre.tex
│   ├── marquetypographique.jpg
│   ├── meta_couverture.md
│   ├── pandoc-quotes.lua
│   ├── parentheses.lua
│   ├── point-final.lua
│   └── typographie.lua
├── .gitlab-ci.yml
├── livre.sh
├── Makefile
└── texte
    ├── 1_meta.md
    └── 2_etc.md
```

#### Makefile

Les différentes options dans le fichier `Makefile` sont :

* `make livre` pour créer un fichier PDF imprimable avec LuaTeX par l’intermédiaire de livre.sh qui crée des notes de bas de page spécifiques pour chaque fichier en fonction du nom du fichier ; cela permet d’éviter des conflits dans le cas où il existerait plusieurs fichiers contenant des notes similaires (plusieurs notes 1, 2, 3 par ex. dans les différents fichiers sources)
* `make couv` pour créer automatiquement une couverture imprimable au format PDF
* `make pdf` pour créer un fichier PDF imprimable avec LuaTeX (sans l’utilisation du script livre.sh et par conséquent sans des notes de bas de page spécifiques pour les différents fichiers sources)
* `make epub` pour créer un livre numérique au format EPUB2
* `make odt` pour créer un fichier texte OpenDocument (LibreOffice)
* `make brut` pour créer un fichier brut au format Markdown (.md)
* `make html` pour créer une page web à partir du livre (elle contient en un seul fichier HTML toutes les données nécessaires, CSS, JavaScript, HTML et images)
* `make web` pour déployer, grâce à GitLab CI et GitLab Pages, le livre au format HTML sur Internet
* `make tex` pour créer un fichier TeX (utile pour produire un fichier PDF sur mesure, notamment pour y appliquer certaines normes de microtypographie avant une compilation par ex. avec `latexmk -luatex`)
* `make cleantex` pour nettoyer le dossier en cas de compilation du fichier TeX
* `make cleanall` pour supprimer tous les fichiers produits par les commandes `make`
* `make tout` pour produire toutes les versions du livre (PDF, couverture, EPUB, ODT, MD, HTML)

#### Les variables

Les variables dans le `Makefile` sont :

* `MEXT = md` pour le type d’extension utilisée (txt, md, etc.)
* `SRC = $(sort $(wildcard texte/*.$(MEXT)))` pour le dossier où se trouve le texte source
* `BIB = bibliographie/sources.bib` pour la source bibliographique (un fichier .bib)
* `CSL = bibliographie/style.csl` pour le style appliqué à la bibliographie et aux notes de bas de page
* `NOM = $(shell basename $(CURDIR))` pour que le fichier produit porte le nom du dossier où il se trouve
* `GABARIT = gabarit/livre.tex` pour l’emplacement du gabarit du livre (le format PDF)
* `GABARIT_COUVERTURE = gabarit/couverture.tex` pour l’emplacement du gabarit de la couverture
* `hauteur = 181mm` pour la hauteur du livre (format PDF)
* `largeur = 121mm` pour la largeur du livre (format PDF)
* `fondperdu = 5mm` pour le fond perdu (il s’applique à la couverture ; il est néanmoins possible d’ajouter manuellement un fond perdu au gabarit du livre)
* `dos = 7mm` pour le dos de la couverture
* `titre_table = Table des matières` pour le titre de la table des matières
* `titre_bibliographie = Bibliographie` pour le titre de la bibliographie
* `police = crimson` pour la police choisie pour le livre (son nom selon les paquets LaTeX disponibles, par ex. librebaskerville, ebgaramond, crimson)
* `policeoptions = lining` pour les options de cette police
* `corps = 12pt` pour déterminer le corps (la taille) de la police

Des variables se trouvent également dans les métadonnées de deux fichiers : `texte/1_meta.md` (des variables générales) et `gabarit/meta_couverture.md` (des variables spécifiques pour la couverture).

Dans le fichier `texte/1_meta.md`, elles sont :

```yml
---
author: Prénom Nom
date: 4 février 2042
depot: Premier trimestre 2019
description: 'Une description de l''ouvrage.'
description_ouvrage_reseau: 'Une indication sur la présence de ce livre sur Internet (en rapport avec la variable lien_ouvrage_reseau).'
details_colophon: 'Un paragraphe descriptif sur quelques détails relatifs à la fabrication de l''ouvrage (en rapport avec la variable lien_colophon).'
identifier:
- scheme: 'ISBN-13'
  text: '978-0-0000-1111-0'
imprimerie: Imprimé en Europe
informations_generales: 'Quelques informations sur le livre, l''auteur, la maison d''édition, etc. (en rapport avec la variable lien_informations_generales)'
lang: fr
licence: 'Cet ouvrage est mis à disposition selon les termes de la Licence Creative Commons Attribution --- Pas d''Utilisation Commerciale --- Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0).'
lien_colophon: 'https://lesitedevotreusinelivre.com/colophon'
lien_informations_generales: 'https://lesitedevotreusinelivre.com/informations'
lien_licence: 'https://lesitedevotreusinelivre.com/partage'
lien_ouvrage_reseau: 'https://lesitedevotreusinelivre.com/la-page-du-livre/'
lieu: 'Usine Livre, Mars-sur-Moselle'
publisher: Usine Livre
rights: '© 2018 Usine Livre, CC BY-NC-SA'
subject: 'un, deux, trois'
title: Le titre de votre livre
version: '1.0'
year: 2042
---
```

Dans le fichier `gabarit/meta_couverture.md`, elles sont :

```yml
---
author: "Prénom Nom"
title: "Le titre de votre livre"
publisher: "Usine Livre"
descriptionquatrieme: "Une description du livre qui figurera sur la quatrième de couverture. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
auteurquatrieme: "Une brève description de l’auteur. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
---
```
Il reste possible d’ajouter d’autres variables avec Pandoc, soit dans les métadonnées, soit au travers de l’option -V, et de les intégrer aux gabarits (présentes sous cette forme `$variable$`).

#### Fichiers à remplacer

Les fichiers à remplacer sont :

- les fichiers dans le dossier `texte` ; il peut n’y avoir qu’un seul fichier comportant les métadonnées à son sommet
- `gabarit/code-barres.png`
- `gabarit/couverture.jpg` (l’image qui se trouvera sur la couverture, il est préférable d’utiliser une image avec au minimum 300 dpi)
- `gabarit/ebook/couverture_ebook.jpg` (dimension 1000x1500px que vous pouvez récupérer avec gimp dans la couverture créée sous format PDF grâce `make couv`)
- `gabarit/marquetypographique.jpg` et `gabarit/html/marquetypographique.jpg` (ce petit détail typographique pour personnaliser votre travail)
- les fichiers dans le dossier `bibliographie` (en cas de leur utilisation)

### Utilisation spécifique des gabarits

Chaque format comporte ses spécificités. Les gabarits des différents formats peuvent être considérablement modifiés. Il existe de nombreux commentaires à cet effet dans les différents fichiers.

#### Livre PDF

Pour produire un livre sous format PDF, il faut utiliser la commande `make pdf`.

Pandoc utilisera LuaTeX pour convertir le texte source en un document PDF grâce au gabarit `gabarit/livre.tex`. Si vous utilisez une ancienne version de Pandoc, il faudra remplacer l’option `--pdf-engine` par `--latex-engine` dans le fichier `Makefile`. Il est néanmoins recommandé d’utiliser des versions à jour de Pandoc et de la distribution LaTeX.

LuaTeX permettra notamment d’avoir une utilisation automatique du français pour la typographie (par ex. transformation des guillemets anglais en guillemets français, ajout d’une espace insécable avant un point d’interrogation, etc.). Il est néanmoins recommandé de faire ces modifications en amont dans le texte source pour que ces règles soient également respectées dans les autres formats (HTML, EPUB, etc.) ; un outil comme Grammalecte peut corriger automatiquement ces détails.

Il est possible de choisir la police du document à partir des variables (par défaut, le Crimson pour le PDF, le EB Garamond pour les autres formats), mais vous pouvez aussi en utiliser d’autres avec le paquet LaTeX *fontspec* (dans le gabarit il faudra décommenter et remplir `setmainfont` et `setsansfont` avec votre choix, notamment des polices OTF présentes sur votre système).

Vous pouvez en outre ajouter des marques d’impression en modifiant le gabarit, notamment pour rogner votre livre après impression ou si votre imprimeur le demande.

Pour ajouter une page blanche, notamment si vous avez besoin d’un document avec un nombre de pages divisible par 4 (nous vous recommandons de les placer à la fin de l’ouvrage, voire au début, en les dispersant afin d’éviter d’en placer plus de 2 d’affilée), vous pouvez ajouter dans le gabarit ou dans le fichier LaTeX produit en cas de modification manuelle cette ligne :

```latex
\clearpage\mbox{}\thispagestyle{empty}\clearpage
```

Une autre option choisie est celle de ne pas placer d’alinéa sous un titre. Pour modifier ce détail, il faut changer la ligne suivante avec l’option *true* dans le gabarit : `\PolyglossiaSetup{french}{indentfirst=false}`.

Pour ajouter une épigraphe, vous pouvez utiliser la commande suivante (définie dans le gabarit), dont le troisième argument peut être laissé vide, après votre titre (avec un saut de ligne entre les deux) :

```latex
\epigraphe{Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.}{Prénom Nom}{L'ouvrage cité}
```

Avec la classe LaTeX utilisée (scrbook), vous pouvez également ajouter cette ligne après votre titre (avec un saut de ligne entre les deux) :

```latex
\vspace{-1sp plus 10pt minus 10pt}
\dictum[Nom Prénom, \textit{L'ouvrage cité}]{Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.}
\vspace{1\baselineskip plus 1sp minus 1sp}
```

Vous pouvez aussi ajouter une épigraphe en début de livre, placée dans une page vide. Pour cela, il faudra utiliser le code suivant :

```latex
\thispagestyle{empty}
\vspace*{4\baselineskip}
\epigraphe{Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.}{Prénom Nom}{L'ouvrage cité}
\clearpage
```

Ou celui-ci :

```latex
\thispagestyle{empty}
\vspace*{4\baselineskip}
\dictum[Lorem Ipsum, \textit{Un livre Ipsum}]{Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.}
\clearpage
```

Pour changer la largeur de l’épigraphe se trouve un passage à modifier au niveau du commentaire « largeur de l’épigraphe » dans le gabarit LaTeX.

Pour ce qui est de la table des matières, le choix n’est pas ici celui d’une démarche universitaire, mais il est possible de transformer la table et de la numéroter jusqu’à 6 niveaux. Nous avons privilégié trois niveaux pour structurer le texte (chapitre, section, sous-section non numérotée).

Il est possible d’ajouter des illustrations avec le paquet `graphicx` voire des pages entières (une illustration pleine page par ex.) avec le paquet `pdfpages`.

Dans le cas d’un recueil de poèmes ou de textes en prose, on peut utiliser une section si on ne veut pas que chaque poème s’ouvre sur la page de droite. Au niveau du texte, avec le langage Markdown, on utilisera donc `##` plutôt que `#` pour les titres (voire un titre vide ou des `\clearpage` dans le cas d’absence de titre). Si vous voulez conserver des titres plus imposants (comme ceux des chapitres), il faudra alors changer dans le gabarit l’option `open=right` en `open=any`. Dans le cas de l’utilisation de chapitre pour les poèmes, il sera utile de commenter la ligne `\renewcommand*{\chapterpagestyle}{empty}` pour avoir des numéros de page à chaque début de poème.

Il faut également pour les poèmes changer `\setlength{\parindent}{1em}` en `\setlength{\parindent}{0em}`, et vous pouvez également avec LaTeX vous amuser avec les \vspace et \hspace et faire des poèmes avec une typographie originale (et peut-être même des calligrammes)

Pour un recueil de poésie, mais également pour un roman ou un essai, vous pouvez remplacer les lignes :

```latex
\lehead{\normalfont\footnotesize\rmfamily\textsc\runtitle}
\rohead{\normalfont\footnotesize\rmfamily\textsc\headmark}
```

par :

```latex
\lehead{}
\rohead{}
```

si vous ne voulez pas d’en-tête, et dans ce cas également changer l’option `headinclude` en `false`.

Pour ajouter le titre d’un chapitre non numéroté dans l’en-tête, dans le cadre de ce gabarit, il est possible d’utiliser la commande `\markright{Le titre}` sous le titre du chapitre lui-même (les options `\markleft{Le titre}` et `\markboth{Le titre}{Le titre}` existent également).

Pour supprimer la ligne séparant le numéro de chapitre du titre de chapitre, il est possible de commenter la commande `\rule` dans la `\renewcommand*{\chapterformat}`.

Il est également possible de remplacer les notes de bas de page par des notes de fin d’ouvrage grâce au paquet `enotez`. Dans ce cas, il faudra décommenter la partie dans le préambule se trouvant sous la mention *notes de fin d’ouvrage*, ainsi que celle dans le corps du document afin de faire apparaître les notes de fin d’ouvrage avec la commande `\printendnotes`.

Afin d’optimiser le [gris typographique](https://fr.wikipedia.org/wiki/Gris_typographique), il est possible, comme l’indique Robert Bringhurst dans *The Elements of Typographic Style*, d’ajuster l’espace entre les mots (ce que fait LaTeX automatiquement), l’espace entre les lettres et la largeur du glyphe. Pour faire varier automatiquement l’espace entre les lettres pour tout le document, un script lua (`gabarit/letterspacing.lua`) peut être utilisé en décommentant la ligne avec la mention *interlettrage* (dans le script, une variation de 0,01ex). Pour une modification spécifique de l’interlettrage, il est possible d’utiliser la commande `textls` fournie par le paquet `microtype` (se référer à [la documentation de ce paquet](https://ctan.org/pkg/microtype)). Afin de faire varier la largeur du glyphe, il existe une option nommée `expansion` dans le paquet `microtype`. En passant cette option à `true`, la largeur du glyphe variera au maximum de 2%. Ces deux dernières modifications provoquent des [avis partagés](http://www.typografi.org/justering/oppfatninger/syn_english.html) parmi les typographes. Elles sont désactivées par défaut dans ce gabarit. Il sera possible de tester le gris typographique en décommentant la partie du gabarit avec la mention *gris typographique*.

LaTeX n’est pas le langage parfait si vous souhaitez rigoureusement respecter une [grille typographique](https://fr.wikipedia.org/wiki/Grille_(mise_en_page)) (surtout lors d’ajouts d’images), il faudra lui préférer ConTeXt qui est plus respectueux de ce principe typographique. Des efforts sont effectués en ce moment à ce niveau avec le paquet LaTeX `returntogrid`. Il reste néanmoins envisageable de respecter une grille grâce au script ci-dessus et à un travail manuel microtypographique sur le fichier TeX (après avoir donc fait un `make tex`) avec des commandes comme `\looseness=-1`ou `\looseness=1` ou celles du paquet `microtype`.

Pour tester la grille, vous pouvez décommenter la partie du gabarit avec la mention *grille typographique* (il existe 2 options utilisables).

Pour centrer un passage, pour l'aligner à droite ou pour l'aligner à gauche (sans justification), sans ajouter un espace vertical et donc en respectant la grille, vous pouvez utiliser les environnements `nscenter`, `nsright`, `nsleft`. Par exemple :

```latex
\begin{nscenter}
Texte à centrer.
\end{nscenter}
```

Pour utiliser des astérismes dans le fichier PDF, il est possible d'utiliser la commande `\asterisme`.

#### Fichier TeX

La production d’un fichier TeX avec `make tex` sera utile afin de produire un fichier PDF sur mesure, notamment pour y appliquer certaines normes de microtypographie. Il pourra ensuite être compilé par ex. avec la commande `latexmk -lualatex fichier.tex`.

#### Couverture PDF

Après avoir modifié les variables relatives à la couverture `gabarit/meta_couverture.md` et remplacé l’image de couverture `gabarit/couverture.jpg`, vous pouvez automatiquement créer avec `make couv` la couverture de votre ouvrage, son dos ainsi que sa quatrième sur un même document PDF. Il est possible de modifier le gabarit de la couverture : `gabarit/couverture.tex`.

Si vous n’avez besoin que de la couverture (première de couverture), vous pouvez simplement ignorer les autres données et récupérer celle-ci en ouvrant et rognant le fichier PDF (contenant quatrième, dos et première de couverture) produit avec Gimp dans une haute résolution.

Pour ce qui est de la composition de cet ensemble, nous vous conseillons vivement (à part bien sûr si vous avez un talent certain de graphiste) de choisir la sobriété, mais une sobriété réfléchie qui vous démarque (par ex. grâce à un choix de couleurs toujours identique, un certain type d’illustration toujours identique pour la couverture, ou une certaine organisation typographique de la couverture). Si vous utilisez de l’impression à la demande, il faut savoir que les couvertures seront imprimées le plus souvent sur du carton blanc, que les couleurs seront moins éclatantes qu’avec de l’offset, que l’impression sera moins précise. Il ne vous reste qu’à jouer avec cela, à utiliser du blanc ou un fond blanc, à préférer des choix simples, la présence d’un logo, d’une police avec une identité forte (sans pour autant qu’elle soit trop originale, *kitsch*), d’un alignement original, etc. Nous vous proposons un exemple de couverture dans le dossier `exemples`, que vous pouvez librement réorganiser.

#### Ebook

Il s’agit ici du format EPUB2 (encore aujourd’hui ce format reste plus utilisé que l’EPUB3). Il est produit avec la commande `make epub`. Même s’il est difficile d’atteindre un formatage identique sur les nombreux appareils lisant de l’EPUB, l’ebook produit devrait être convenablement lisible sur la plupart des plates-formes.

Les fichiers concernant ce gabarit se trouvent dans `gabarit/ebook`, notamment la feuille de style `ebook.css`.

Pour la production du fichier EPUB, il est possible d’intégrer des balises HTML dans le texte source (par ex. un paragraphe avec une classe qui aligne à droite, un paragraphe avec une marge à gauche différente, comme dans notre gabarit `<p class="a-droite epigraphe">épigraphe</p>`).

Pour la couverture dans l’EPUB (qui se trouve dans `gabarit/ebook`), il est nécessaire de garder en mémoire qu’elle s’affichera différemment sur différentes liseuses. Certaines liseuses la déformeront légèrement. Notre expérience nous fait préférer une couverture de 1000px par 1500px pour un affichage avec une déformation moindre. Il ne s’agit donc que de la couverture pour l’EPUB (la couverture promotionnelle, celle se trouvant sur les différentes boutiques, a une résolution plus grande, chaque plate-forme fixe son minimum, nous utilisons à titre indicatif pour celle-ci une résolution de 3200px x 4800px).

Il est possible de remplacer, dans le fichier EPUB contenant des notes de fin de chapitre, les flèches standards par un autre caractère, grâce au script `gabarit/ebook/modification.sh` ; par défaut, il remplacera les `↩︎` par des `↑` précédés d'une espace insécable. 

Nous avons ajouter une phrase au gabarit qui précise que le fichier ne contient ni DRM ni tatouage numérique. Soyez chics, faites en sorte que cette affirmation soit vraie...

#### HTML

Le livre numérique dans son format HTML est créé avec `make html` ; il s’agit d’un seul fichier HTML contenant toutes les données nécessaires (CSS, JavaScript, images) grâce à l’option Pandoc `--self-contained` qui utilise dans le document HTML des `data:`.

Pour ajouter une numérotation aux différents chapitres, il faut ajouter dans le `Makefile` l’option Pandoc `--number-sections`. Dans cette même commande du fichier `Makefile`, il est précisé une modification des flèches de retour dans les notes de bas de page (un remplacement des `↩︎` par des `↑`).

Si vous souhaitez une alternative, la feuille de style *Tufte* peut peut-être vous convenir.

La commande `make web` permet de déployer le livre numérique au format HTML sur Internet grâce à GitLab CI et GitLab Pages (par l'emploi du fichier [.gitlab-ci.yml](./.gitlab-ci.yml)). Dans notre cas, la page du livre numérique se trouve à cette adresse : [https://cestabrupt.gitlab.io/gabarit-abrupt/](https://cestabrupt.gitlab.io/gabarit-abrupt/) (cette adresse s'adaptera à votre dépôt GitLab).

#### Autres

##### ODT

Vous pouvez créer un fichier texte OpenDocument avec `make odt`. Il est envisageable d’appliquer différents styles aux titres et d’insérer une table des matières.

##### Fichier brut

Vous pouvez créer un fichier brut avec `make brut`. Le formatage se fait ici avec le langage de balisage léger Markdown, qui met donc en évidence les italiques et les notes de bas de page. Il est néanmoins possible d’en choisir un autre selon les options accordées par Pandoc (par ex. AsciiDoc).

Un retour à la ligne sera également effectué au 72e caractère ; il est toutefois envisageable d’utiliser l’option --wrap de Pandoc pour faire figurer tous les paragraphes sur une même ligne.

##### Extensibilité

Avec Pandoc, vous pouvez aussi convertir vos fichiers sources en de nombreux formats (par ex. en ConTeXt, document word, etc.).

## Quelques conseils graphiques et typographiques pour la composition des différents livres

Voilà cinq conseils généraux pour s’améliorer en design (et peut-être en bien d’autres choses)... Le premier est de développer votre curiosité, d’essayer de comprendre le « comment » et le « pourquoi » de tous les détails qui composent un livre, un objet, ce que vous êtes en train de fabriquer (par ex. pourquoi un dos à la française et pas à l’américaine ?). Le deuxième est de copier, de voler, de vous approprier et de transformer les designs que vous aimez, c’est ainsi que les plus belles choses, de l’artisanat à l’art, se fabriquent. Le troisième conseil est la cohérence adaptative, il ne faut pas vous cloisonner dans une rigidité contraignante, mais choisir des lignes directrices souples qui animeront l’ensemble de votre création (le choix d’une palette de couleurs, d’une police, d’un détail revenant à chaque fois dans une nouvelle création comme une signature, etc.). Le quatrième est la simplicité, le principe KISS, le rasoir d’Ockham, la constante recherche d’une voie directe pour résoudre un problème, ce qui ne veut pas dire forcément « minimalisme » (une œuvre baroque peut être simple, animée par une expression limpide). Et le dernier conseil serait d’échouer, d’échouer et de recommencer sans cesse, de hacker sans cesse votre environnement, de fortifier par l’acte de faire une confiance DIY, libre, et d’échouer et de recommencer encore, de croire en la construction de votre expérience. Cela forgera votre œil. Le reste n’est qu’une accumulation d’apprentissages techniques.

Pour revenir à la fabrication du livre, et plus précisément à sa forme papier, il est important de choisir de façon réfléchie le format de l’ouvrage. Nous vous déconseillons de choisir des dimensions connues comme celles de l’A5 ou de l’A4 (l’œil y percevra inconsciemment quelque chose de commun, de qualité moindre), mais nous vous conseillons de choisir un format que vous aimez (plutôt vertical, plutôt carré, etc.) et surtout qui convient au contenu de votre ouvrage (sans être impossible, un livre de photographie sera difficilement envisageable dans un format trop vertical). Nous vous recommandons le format 2:3, qui est peut-être le plus adaptable à différents contenus, si vous appréciez la proportion plus « carrée » des A5, A4, etc., vous pouvez choisir un format 1:√2 en évitant des dimensions trop proches de celles [ISO 216](https://fr.wikipedia.org/wiki/ISO_216), si vous souhaitez quelque chose de plus vertical, un format 1:√3 peut sans doute convenir.

La composition interne du livre suit également [certaines règles](https://en.wikipedia.org/wiki/Canons_of_page_construction), qui sont automatisées dans notre gabarit par la classe LaTeX *Koma* (il reste possible de la changer légèrement grâce à l’option `DIV`, voir notamment à ce sujet la [partie 2.2 du guide de cette classe](https://ctan.org/pkg/scrbook)) ; la composition se construira autour de la *section dorée* en créant un équilibre entre les marges, notamment extérieures et intérieures (il ne faut pas oublier d’ajouter à la marge intérieure une correction si l’ouvrage est relié).

En ce qui concerne le choix de la police de caractères, nous avons opté pour une seule police afin d’instaurer une unité visuelle (de la couverture aux titres en passant par le contenu, en jouant sur le corps ou l’italique de la police). Nous vous recommandons vivement de choisir une police avec empattement, en anglais *serif*, pour le contenu (elle facilite la lecture sur de longs textes, tout comme le choix d’un papier crème plutôt que d’un papier blanc).

Si vous souhaitez différencier les polices de titre, de contenu et de couverture (une pour chaque, nous vous recommandons de ne pas dépasser trois polices, et même de préférer deux polices, le choix d’une même police pour la couverture et les titres), il est préférable de choisir des polices avec un style complémentaire. À cette fin, voici quelques conseils (en anglais) : quelques [recommandations](https://www.typography.com/techniques/), un choix de combinaisons de polices libres [ici](https://www.creativebloq.com/typography/20-perfect-type-pairings-3132120) ou [là](https://fontpair.co/#featured-pairs), ou encore cet [outil](https://fontjoy.com/) pour tester différentes combinaisons.

Vous pouvez coupler par ex. un Garamond pour le texte lui-même avec un Bodoni pour les titres, ou utiliser uniquement un Garamond et jouer sur une graisse (gras) différente pour les titres, voire une police sans empattement comme un Helvetica.

Il existe de nombreuses polices libres de qualité, voilà par exemple [une liste de polices avec empattement](http://www.tug.dk/FontCatalogue/seriffonts.html).

Nous avons choisi ici un [Crimson](https://github.com/skosch/Crimson), belle police libre (sic) que nous utilisons pour [nos ouvrages](https://abrupt.cc/colophon/), et un Garamond avec la police libre [EB Garamond](https://github.com/georgd/EB-Garamond), qui confèrent aux différents documents un côté « classique ». Lors de votre choix, nous vous conseillons de préférer les formats OTF (et WOFF pour le web).

Lors de la composition de votre ouvrage, vous pouvez même éviter d’utiliser différentes graisses simplement en jouant avec la taille de votre police (le corps). Il reste préférable de ne pas utiliser plus de 3 corps différents, tant au niveau du contenu que des titres (pour créer des repères, pour éviter que l’œil se disperse).

Un autre conseil est celui d’éviter de placer des espaces entre les paragraphes comme alternative à l’indentation. Cela pourrait créer une confusion lorsqu’une phrase commence en début de page (et que la dernière phrase en fin de page précédente se termine à la marge) ; il est alors difficile de savoir s’il s’agit d’un nouveau paragraphe.

Il existe de nombreux détails typographiques qui apportent un certain caractère à votre ouvrage (que vous pourrez facilement ajouter avec LaTeX) comme l’utilisation en début de chapitre des lettrines. Nous n’utilisons pas ce genre d’effet, lui trouvant quelques lourdeurs. Par contre, quelque chose d’élégant que nous n’utilisons pas dans ces gabarits est l’utilisation de petites majuscules pour les premiers mots de la première ligne du premier paragraphe d’un chapitre ; avec LaTeX, vous pouvez utiliser `scshape` ou `textsc` pour cela (il est bien sûr possible d’automatiser cet effet avec LaTeX) :

```latex
\noindent \textsc{Lorem ipsum dolor} sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
```

La couverture est ici automatisée, mais il est souvent plus judicieux de faire des ajustements manuels, notamment pour un saut de ligne, des espacements, etc.

En ce qui concerne l’illustration, sans avoir besoin de talent d’illustrateur, vous pouvez créer de façon astucieuse une couverture de qualité. Dans ce but, vous pouvez vous amuser à faire par ex. des variations [« à la joy division »](https://mathematica.stackexchange.com/questions/185200/unknown-graphics-pleasure) (avec un autre programme / langage, de préférence libre), à coder avec LaTeX votre couverture, avec [TikZ](http://pgfplots.net/) (un ex. de [LaTeX Art](https://adamdedwards.com/blog/2018/2/3/latex-art)), faire une couverture avec une image en ASCII, jouer avec une illustration calligraphique, etc.

Quelques dernières précisions sur les détails graphiques utilisés dans ces gabarits :

* Le code-barres peut être fabriqué avec Zint Barcode Studio ; il est ici celui de notre premier ouvrage (Simone Weil, L’Espagnole, Abrüpt).
* La [marque typographique](https://fr.wikipedia.org/wiki/Marque_d%27imprimeur) est ici un dessin de Franz Kafka ; il se trouve donc dans le domaine public, libre à vous de le réutiliser (il en a fait quelques autres dans ce style).
* L’*enso* utilisé sur la couverture vient de cette [image](https://commons.wikimedia.org/wiki/File:Bankeitumblr_lm43mrAJZc1qzgealo1_1280Sakyamuni%26Maitreya.jpg) du domaine public, que nous avons rapidement travaillée avec gimp (désaturation, changement des niveaux, suppression des pixels en trop)
* Avec l’apparition de l’impression à la demande, vous pouvez aisément créer votre petite fabrique d’ouvrage DIY. Si vous souhaitez utiliser un élément graphique de nos gabarits, nos gabarits eux-mêmes, voire réutiliser ce nom d’« Usine Livre », une variation pour votre signature, etc., libre à vous ! Cela nous fera plaisir !
* Le texte est le début de [La Recherche](https://fr.wikipedia.org/wiki/%C3%80_la_recherche_du_temps_perdu) dans lequel nous avons aléatoirement inséré des notes (un sacrilège...)

### Découvertes typographiques et graphiques

Voilà quelques recommandations pour étudier ces questions typographiques et graphiques :

* Jan Tschichold, Livre et typographie, Allia ; un recueil d’articles d’un célèbre typographe
* Robert Bringhurst, The Elements of Typographic Style, Hartley & Marks ; un livre d’un typographe contemporain
* Josef Müller-Brockmann, Systèmes de grille, Entremonde ; un livre sur la grille en typographie
* Jean-Pierre Lacroux, Orthotypographie ; un dictionnaire détaillé sur l’orthographe et la typographie française, ouvrage sous licence de libre diffusion : [Orthotypographie](http://www.orthotypographie.fr/intros/index.html)
* Lexique des règles typographiques en usage à l’Imprimerie nationale, Imprimerie nationale ; ouvrage regroupant les règles typographiques utilisées par l’Imprimerie nationale
* [TeX StackExchange](https://tex.stackexchange.com/) ; un des forums TeX les plus fournis (la source de nombreuses améliorations du gabarit LuaTeX)
* [Logos By Nick](https://www.youtube.com/logosbynick) ; une chaîne YouTube contenant de nombreux tutoriels sur Gimp et Inkscape
* [Thinking with type](http://thinkingwithtype.com/) ; des propos sur la typographie notamment numérique
* [Butterick’s Practical typography](https://practicaltypography.com/) ; un guide pratique pour découvrir la typographie
* [Degreeless.design](https://www.degreeless.design/) ; de nombreuses informations sur le design graphique
* [Canons of page construction](https://en.wikipedia.org/wiki/Canons_of_page_construction) ; la page anglaise Wikipédia qui traite de la construction typographique d’une page

## Créateur

* **Abrüpt** ([abrupt.cc](https://abrupt.cc))

Vous pouvez également [nous contacter](https://abrupt.cc/contact) par courriel ou sur les différents réseaux sociaux mentionnés sur cette page.

## Licence

Ce projet est placé sous licence MIT - voir [LICENSE.md](LICENSE.md) à cet effet.

Les scripts ou bouts de code, découverts sur le réseau, portent la mention de leur source en commentaire.

## Prochainement

* Amélioration du gabarit HTML (notamment pour l’appareil critique)
* Script pour améliorer les espaces insécables entre tirets
* Gabarit Libreoffice
* Un générateur automatique de couverture (« à la Joy Division »)
* La piraterie littéraire n’est jamais finie...

